<?php
require_once 'lib/nusoap.php';
define('HOST', 'localhost');
define('USER', 'root');
define('USER_PASS', 'mysqlroot');
define('DB_NAME', 'Pedidos');

define('ER_FAILED_TO_INSERT_RECORD', -1);
define('INVALID_CLIENT', -101);
define('INVALID_REQUEST', -201);
define('NOT_FOUND', -1000);

function OpenDB()
{
    $connection = new mysqli(HOST, USER, USER_PASS, DB_NAME);
    return $connection;
}
function print_no_contents($arr) {
        foreach ($arr as $k=>$v) {
            echo $k."=> ";
            if (is_array($v)) {
                echo "\n";
                print_no_contents($v);
            }
            else echo "[" . $v . "]";
            echo "\n";
        }
}

function to_request_array($items)
{
    $elements = array();
    if (isset($items['item']['IdRequest'])) {
        $elements[] = array (
            'IdRequest' => $items['item']['IdRequest'],
            'IdClient' => $items['item']['IdClient'],
            'RequestDate' => $items['item']['RequestDate']
        );
    } else if (isset($items['item'])) {
        foreach ($items['item'] as $c) {
            $elements[] = array (
                'IdRequest' => $c['IdRequest'],
                'IdClient' => $c['IdClient'],
                'RequestDate' => $c['RequestDate']
            );
        }
    }

    return $elements;
}


function to_detail_array($items)
{
    $elements = array();
    if (isset($items['item']['IdDetail'])) {
        $elements[] = array (
            'IdDetail' => $items['item']['IdDetail'],
            'IdRequest' => $items['item']['IdRequest'],
            'IdProduct' => $items['item']['IdProduct'],
            'Amount' => $items['item']['Amount']
        );
    } else if (isset($items['item'])) {
        foreach ($items['item'] as $c) {
            $elements[] = array (
                'IdDetail' => $c['IdDetail'],
                'IdRequest' => $c['IdRequest'],
                'IdProduct' => $c['IdProduct'],
                'Amount' => $c['Amount']
            );
        }
    }

    return $elements;
}

function findRequest($IdClient, $RequestDate, $mysqli)
{
    $mysqli->query("SET NAMES UTF8");
    $idClient = utf8_encode(addslashes($IdClient));
    $date = utf8_encode(addslashes($RequestDate));
    $query  = "SELECT IdPedido FROM Pedido WHERE IdCliente = " . $idClient
        . " AND Fecha = '" . $date . "'";

    $result = $mysqli->query($query);
    if ($result->num_rows == 0)
        return  NOT_FOUND;

    $row = $result->fetch_assoc();
    return $row['IdPedido'];
}

function PutRegisters($RequestItems, $DetailItems)
{
    if (is_array($RequestItems)) {
        $requests = to_request_array($RequestItems);
    } else {
        $requests = array();
    }

    if (is_array($DetailItems)) {
        $details = to_detail_array($DetailItems);
    } else {
        $details = array();
    }

    $mysqli = OpenDB();
    foreach ($requests as $r) {
        $idRequest = $r['IdRequest'];
        $requestIdClient = $r['IdClient'];
        $requestDate = $r['RequestDate'];

        if (findRequest($requestIdClient, $requestDate, $mysqli) == NOT_FOUND) {
            $newIdRequest = CreateRequest($requestIdClient, $requestDate,
                                          $mysqli);
            foreach ($details as $d) {
                $product = $d['IdProduct'];
                $amount = $d['Amount'];

                $detailIdRequest = $d['IdRequest'];
                if ($idRequest != $detailIdRequest) continue;

                CreateDetail($newIdRequest, $product, $amount, $mysqli);
            }
        }
    }

    $mysqli->close();

    return count($requests) + count($details);
}



function CreateClient($Name, $MiddleName, $LastName, $Address, $mysqli)
{
    $mysqli->query("SET NAMES UTF8");
    $name = utf8_encode(addslashes($Name));
    $middleName = utf8_encode(addslashes($MiddleName));
    $lastName = utf8_encode(addslashes($LastName));
    $address = utf8_encode(addslashes($Address));
    $query  = "INSERT INTO Cliente(Nombre, ApellidoPaterno, ApellidoMaterno, "
        . "Domicilio) VALUES('" . $name . "', '" . $middleName . "', "
        . "'" . $lastName . "', '" . $address . "')";

    $mysqli->query($query);
    $id = $mysqli->insert_id;

    if ($id == 0) {
        return ER_FAILED_TO_INSERT_RECORD;
    }

    return $id;
}


function CreateRequest($IdClient, $RequestDate, $mysqli)
{
    $mysqli->query("SET NAMES UTF8");
    $idClient = utf8_encode(addslashes($IdClient));
    $date = utf8_encode(addslashes($RequestDate));
    $query  = "INSERT INTO Pedido(IdCliente, Fecha) "
        . "VALUES(" . $idClient . ", '" . $date . "')";

    $mysqli->query($query);
    $id = $mysqli->insert_id;

    if ($id == 0) {
        return ER_FAILED_TO_INSERT_RECORD;
    }

    return $id;
}

function CreateDetail($IdRequest, $IdProduct, $Amount, $mysqli)
{
    $mysqli->query("SET NAMES UTF8");
    $idRequest = utf8_encode(addslashes($IdRequest));
    $idProduct = utf8_encode(addslashes($IdProduct));
    $amount = utf8_encode(addslashes($Amount));
    $query  = "INSERT INTO Detalle(IdPedido, IdProducto, Cantidad) "
        . "VALUES(" . $idRequest . ", '" . $idProduct . "', " . $amount . ")";

    $mysqli->query($query);
    $id = $mysqli->insert_id;

    if ($id == 0) {
        return ER_FAILED_TO_INSERT_RECORD;
    }

    return $id;
}

function GetAllClients()
{
    $mysqli = OpenDB();
    $mysqli->query("SET NAMES UTF8");
    $query = "SELECT * FROM Cliente";

    $result = $mysqli->query($query);
    $mysqli->close();
    if ($result->num_rows == 0) {
        return array();
    }

    $clients = array();
    while ($row = $result->fetch_assoc()) {
        $clients[] = array(
            'IdClient' => $row['IdCliente'],
            'Name' => $row['Nombre'],
            'MiddleName' => $row['ApellidoPaterno'],
            'LastName' => $row['ApellidoMaterno'],
            'Address' => $row['Domicilio']);
    }

    return $clients;
}

function GetAllRequests()
{
    $mysqli = OpenDB();
    $mysqli->query("SET NAMES UTF8");
    $query = "SELECT * FROM Pedido";

    $result = $mysqli->query($query);
    $mysqli->close();
    if ($result->num_rows == 0) {
        return array();
    }

    $requests = array();
    while ($row = $result->fetch_assoc()) {
        $requests[] = array(
            'IdRequest' => $row['IdPedido'],
            'IdClient' => $row['IdCliente'],
            'RequestDate' => $row['Fecha']);
    }

    return $requests;
}

function GetAllDetails()
{
    $mysqli = OpenDB();
    $mysqli->query("SET NAMES UTF8");
    $query = "SELECT * FROM Detalle";

    $result = $mysqli->query($query);
    $mysqli->close();
    if ($result->num_rows == 0) {
        return array();
    }

    $details = array();
    while ($row = $result->fetch_assoc()) {
        $details[] = array(
            'IdDetail' => $row['IdDetalle'],
            'IdRequest' => $row['IdPedido'],
            'Item' => $row['Articulo'],
            'Amount' => $row['Cantidad']
        );
    }

    return $details;
}

function GetAllProducts()
{
    $mysqli = OpenDB();
    $mysqli->query("SET NAMES UTF8");
    $query = "SELECT * FROM Producto";

    $result = $mysqli->query($query);
    $mysqli->close();
    if ($result->num_rows == 0) {
        return array();
    }

    $products = array();
    while ($row = $result->fetch_assoc()) {
        $products[] = array(
            'IdProduct' => $row['IdProducto'],
            'Description' => $row['Descripcion']
        );
    }

    return $products;
}


function GetAllRequestsWithDetails()
{
    $mysqli = OpenDB();
    $mysqli->query("SET NAMES UTF8");
    $query = "SELECT Pedido.IdPedido as IdPedido, Nombre, ApellidoPaterno, "
        . "ApellidoMaterno, Articulo, Cantidad, Fecha  FROM Cliente, "
        . "Pedido, Detalle WHERE Pedido.IdCliente = Cliente.Idcliente AND "
        . "Pedido.IdPedido = Detalle.IdPedido";

    $result = $mysqli->query($query);
    $mysqli->close();
    if ($result->num_rows == 0) {
        return array();
    }

    $requests = array();
    while ($row = $result->fetch_assoc()) {
        $full_name = $row['Nombre'] . " " . $row['ApellidoPaterno']
            . " " . $row['ApellidoMaterno'];
        $requests[] = array(
            'IdRequest' => $row['IdPedido'],
            'FullName' => $full_name,
            'Item' => $row['Articulo'],
            'Amount' => $row['Cantidad'],
            'RequestDate' => $row['Fecha']);
    }

    return $requests;
}

$server = new soap_server();
$server->configureWSDL("Request", "urn:Request");
$server->soap_defencoding = 'utf-8';

$server->wsdl->addComplexType('Detail', 'complexType', 'struct', 'all', '',
    array(
        'IdDetail' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'IdRequest' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'IdProduct' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'Amount' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1')
        )
    );

$server->wsdl->addComplexType('Request', 'complexType', 'struct', 'all', '',
    array(
        'IdRequest' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'IdClient' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'RequestDate' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1')
        )
    );

$server->wsdl->addComplexType('Product', 'complexType', 'struct', 'all', '',
    array(
        'IdProduct' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'Description' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1')
        )
    );

$server->wsdl->addComplexType('Client', 'complexType', 'struct', 'all', '',
    array(
        'IdClient' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'Name' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'MiddleName' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'LastName' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'Address' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1')
        )
    );


$server->wsdl->addComplexType('RequestDetail', 'complexType', 'struct','all','',
    array(
        'IdRequest' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'FullName' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'Item' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'Amount' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'RequestDate' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1')
        )
    );




$server->wsdl->addComplexType(
    'ArrayOfClient',
    'complexType',
    'array',
    'sequence',
    '',
    array(
        'item' => array(
            'type'      => 'tns:Client',
            'minOccurs' => '0', 
            'maxOccurs' => 'unbounded'
        )
    )
);

$server->wsdl->addComplexType(
    'ArrayOfRequestDetail',
    'complexType',
    'array',
    'sequence',
    '',
    array(
        'item' => array(
            'type'      => 'tns:RequestDetail',
            'minOccurs' => '0', 
            'maxOccurs' => 'unbounded'
        )
    )
);

$server->wsdl->addComplexType(
    'ArrayOfRequest',
    'complexType',
    'array',
    'sequence',
    '',
    array (
        'item' => array (
            'type'      => 'tns:Request',
            'minOccurs' => '0', 
            'maxOccurs' => 'unbounded'
        )
    )
);

$server->wsdl->addComplexType(
    'ArrayOfProduct',
    'complexType',
    'array',
    'sequence',
    '',
    array (
        'item' => array (
            'type'      => 'tns:Product',
            'minOccurs' => '0', 
            'maxOccurs' => 'unbounded'
        )
    )
);

$server->wsdl->addComplexType(
    'ArrayOfDetail',
    'complexType',
    'array',
    'sequence',
    '',
    array (
        'item' => array (
            'type'      => 'tns:Detail',
            'minOccurs' => '0', 
            'maxOccurs' => 'unbounded'
        )
    )
);


$server->register('GetAllClients',
    array(),
    array('clients' => 'tns:ArrayOfClient'),
    'Request',
    false,
    'rpc',
    'literal',
    'Retrieve all clients.'
);

$server->register('GetAllRequests',
    array(),
    array('requests' => 'tns:ArrayOfRequest'),
    'Request',
    false,
    'rpc',
    'literal',
    'Retrieve all requests.'
);

$server->register('GetAllDetails',
    array(),
    array('details' => 'tns:ArrayOfDetail'),
    'Request',
    false,
    'rpc',
    'literal',
    'Retrieve all details.'
);

$server->register('GetAllProducts',
    array(),
    array('details' => 'tns:ArrayOfProduct'),
    'Request',
    false,
    'rpc',
    'literal',
    'Retrieve all products.'
);

$server->register('GetAllRequestsWithDetails',
    array(),
    array('requests' => 'tns:ArrayOfRequestDetail'),
    'Request',
    false,
    'rpc',
    'literal',
    'Retrive all requests with details'
);

$server->register('PutRegisters',
    array(
        'RequestItems' => 'tns:ArrayOfRequest',
        'DetailItems' => 'tns:ArrayOfDetail'),
    array('status' => 'xsd:integer'),
    'Request',
    false,
    'rpc',
    'literal',
    'Receive information from the client and put them in the database.'
);

if (isset($HTTP_RAW_POST_DATA)) {
    $server->service($HTTP_RAW_POST_DATA);
} else {
    $server->service("php://input");
}

?>


