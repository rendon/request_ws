Request WS
==========
A simple Web service to manage buys, to be consumed by my other applications [Requisition_WP8](https://bitbucket.org/rendon/requisition_wp8), [Requisition_BB10](https://bitbucket.org/rendon/requisition_bb10) and [Requisition_Android](https://bitbucket.org/rendon/requisition_android).

This is a college project.

For more information see the project write up (in Spanish): [http://rendon.x10.mx/?p=875](http://rendon.x10.mx/?p=875).

License
=======
This work is under GPLv3.
